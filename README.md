# test-harness-ref

Reference architecture for a FastApi test harness. Front-end will be vanilla js. Back-end will be python. Communication will be RESTful. Target will be to test a faux-Microservice, end-to-end.